# README #

This file contains the R functions described in my recent books:

Wilcox, R. R. (2017).  Understanding and Applying Basic Statistical Methods Using R}. 
New York: Wiley. ISBN: 978-1-119-06139-7.
http://www.wiley.com/WileyCDA/WileyTitle/productCd-1119061393.html

 Wilcox, R. R. (2017). Introduction to Robust Estimation and
Hypothesis Testing 4th Edition. San Diego, CA: Academic Press.  ISBN: 978-0-12-386983-8
https://www.elsevier.com/books

Wilcox, R. R. (in press).  Modern Statistics for the Social and Behavioral Sciences: 
A Practical Introduction} 2nd Ed New York: Chapman & Hall/CRC press.
ISBN: 978-1-4987-9678-1     Cat #: K30405

### What is this repository for? ###

Installing these functions into your version of R provides access to approximately 1400 functions for
applying modern robust methods for comparing groups, studying associations, dealing with various 
multivariate issues, etc. These modern methods provide the possibility of more power relative to classic 
techniques when there is non-normality or heteroscedasticity.

### How do I get set up? ###

Down load the file then use the R command

source(file.choose())

This will open a window listing the files in your computer.
Click on Rallfun, hit enter, and the functions will become part of your version of R 

### Contribution guidelines ###

My books describe the details of these functions. Typing the name of a function and hitting return, 
you will a brief description of what the function does. (They appear at the top of the code.

### Who do I talk to? ###

Contact rwilcox@usc.edu if problems are encountered.